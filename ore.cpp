#include <iostream>
#include <cmath>
const double eps = 1e-9;
using namespace std;
int main() {
	long long n, a = 0, d;
	long double hsum = 0.0;
	cin >> n;
	long double sq = sqrt(n);
	for (long long i = 1; i <= sq; i++) {
		if (n % i == 0) {
			hsum += 1.0 / i;
			a++;
			d = n / i;
			if (d != i) {
				hsum += 1.0 / d;
				a++;
			}
		}
	}
	long double hmean = a / hsum;
	if (abs(hmean - round(hmean)) < eps) cout << "YES";
	else cout << "NO";
	return 0;
}
